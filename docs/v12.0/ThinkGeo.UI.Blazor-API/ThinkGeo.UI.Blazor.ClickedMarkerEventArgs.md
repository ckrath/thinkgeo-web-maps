# ClickedMarkerEventArgs


## Inheritance Hierarchy

+ `Object`
  + `EventArgs`
    + **`ClickedMarkerEventArgs`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`ClickedMarkerEventArgs(Marker,Single,Single,Double,Double)`](#clickedmarkereventargsmarkersinglesingledoubledouble)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`ClickedMarker`](#clickedmarker)|[`Marker`](ThinkGeo.UI.Blazor.Marker.md)|Gets the original target.|
|[`ScreenX`](#screenx)|`Single`|Gets x-coordinate of screen.|
|[`ScreenY`](#screeny)|`Single`|Gets y-coordinate of screen.|
|[`WorldX`](#worldx)|`Double`|Gets x-coordinate of world.|
|[`WorldY`](#worldy)|`Double`|Gets y-coordinate of world.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`ClickedMarkerEventArgs(Marker,Single,Single,Double,Double)`](#clickedmarkereventargsmarkersinglesingledoubledouble)|

### Protected Constructors


### Public Properties

#### `ClickedMarker`

**Summary**

   *Gets the original target.*

**Remarks**

   *N/A*

**Return Value**

[`Marker`](ThinkGeo.UI.Blazor.Marker.md)

---
#### `ScreenX`

**Summary**

   *Gets x-coordinate of screen.*

**Remarks**

   *N/A*

**Return Value**

`Single`

---
#### `ScreenY`

**Summary**

   *Gets y-coordinate of screen.*

**Remarks**

   *N/A*

**Return Value**

`Single`

---
#### `WorldX`

**Summary**

   *Gets x-coordinate of world.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `WorldY`

**Summary**

   *Gets y-coordinate of world.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


